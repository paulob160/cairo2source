REM Backup Cairo2 helper files

echo off
echo "Backing up Cairo helpers..."

cp CairoEx1.c     CairoEx1%1.c
cp CairoHelpers.c CairoHelpers%1.c
cp CairoHelpers.h CairoHelpers%1.h

attrib +R  CairoEx1%1.c
attrib +R  CairoHelpers%1.c
attrib +R  CairoHelpers%1.h

echo "Finished backing up Cairo helpers..."
