/*****************************************************************************/
/* Cairo Vector Graphics Example :                                           */
/*  - using precompiled '.dll' and '.lib'                                    */
/*****************************************************************************/

#include <windows.h>
#include <cairo-features.h>
#include <cairo-win32.h>
#include "CairoHelpers.h"

/*****************************************************************************/

#define TITLE TEXT("Cairo test")
#define BORDER_WIDTH    ((cairoDouble_t)75.0)
#define SNIPPET_WIDTH  ((cairoDouble_t)300.0)
#define SNIPPET_HEIGHT ((cairoDouble_t)300.0)
#define FONT_SIZE       ((cairoDouble_t)24.0)

/*****************************************************************************/

static void on_paint(HDC hdc)
  {
  /*****************************************************************************/

  cairo_surface_t        *surface       = NULL;
  cairo_t                *cr            = NULL;
  double                  line_width    = 0.0;
  cairo_font_extents_t    font_extents;
  cairoHelperErrorCode_t  helperError   = CAIRO_HELPER_ERROR_CODE_NONE;

  cairoPenColours_t       penColour     = CAIRO_PEN_ATTRIBUTE_COLOUR_NONE;
  cairoPenIntensities_t   penIntensity  = CAIRO_PEN_ATTRIBUTE_LIGHT;
  cairoPenRBGA_t          penRGBA;

  /*****************************************************************************/

  surface = cairo_win32_surface_create(hdc);
  cr      = cairo_create(surface);

  line_width = cairo_get_line_width(cr);

  /*****************************************************************************/
  /* Create a new "rectangle" pen state                                        */
  /*****************************************************************************/

  /* Draw a box bordering the snippet */
  helperError = cairoHelperSetCurrentPenColour((cairo_t                  * const)                        cr,
                                               (cairoSurfaceAttributes_t * const)                       &cairoSurfaceAttributes,
                                                                          (const cairoPlotItem_t)        CAIRO_PLOT_ITEM_RECTANGLE,
                                                                          (const cairoPenColours_t)      CAIRO_PEN_ATTRIBUTE_COLOUR_INDIGO,
                                                                          (const cairoPenIntensities_t)  CAIRO_PEN_ATTRIBUTE_LIGHT,
                                                                                                         CAIRO_PEN_NEARLY_TRANSPARENT);

  cairo_rectangle(cr,
                  BORDER_WIDTH  - (line_width / 2.0), BORDER_WIDTH  - (line_width / 2.0),
                  SNIPPET_WIDTH +  line_width,        SNIPPET_WIDTH + line_width);
  cairo_stroke(cr);

  /*****************************************************************************/
  /* Leave the "rectangle" pen state alone and create a new one for the "text" */
  /*****************************************************************************/

  /* And some text...of colour ? */
  helperError = cairoHelperSetCurrentPenColour((cairo_t                  * const)                        cr,
                                               (cairoSurfaceAttributes_t * const)                       &cairoSurfaceAttributes,
                                                                          (const cairoPlotItem_t)        CAIRO_PLOT_ITEM_TEXT,
                                                                          (const cairoPenColours_t)      CAIRO_PEN_ATTRIBUTE_COLOUR_BLUE,
                                                                          (const cairoPenIntensities_t)  CAIRO_PEN_ATTRIBUTE_NORMAL,
                                                                                                         CAIRO_PEN_HALF_OPAQUE);
   
  if (helperError != CAIRO_HELPER_ERROR_CODE_NONE)
    {
    while (true)
      ;
    }

  cairo_select_font_face(cr,
                         "Arial",
                         CAIRO_FONT_SLANT_NORMAL,
                         CAIRO_FONT_WEIGHT_BOLD);

  cairo_set_font_size(cr, FONT_SIZE);

  cairo_font_extents(cr, &font_extents);

  cairo_move_to(cr,
                BORDER_WIDTH,
                BORDER_WIDTH + SNIPPET_WIDTH + font_extents.ascent);

  cairo_show_text(cr, "This is some example text!");

  /*****************************************************************************/
  /* Check the "rectangle" pen state has been preserved                        */
  /*****************************************************************************/

  cairoHelperReadCurrentPenColour( (cairo_t                  * const)                  cr,
                                   (cairoSurfaceAttributes_t * const)                 &cairoSurfaceAttributes,
                                                              (const cairoPlotItem_t)  CAIRO_PLOT_ITEM_RECTANGLE,
                                   (cairoPenColours_t        * const)                 &penColour,
                                   (cairoPenIntensities_t    * const)                 &penIntensity,
                                   (cairoPenRBGA_t           * const)                 &penRGBA);

  /*****************************************************************************/
  /* Then create a new "rectangle" pen state. THIS OVERWRITES (DESTROYS) THE   */
  /* OLD PEN STATE!                                                            */
  /*****************************************************************************/

  helperError = cairoHelperSetCurrentPenColour((cairo_t *const)                 cr,
                                              (cairoSurfaceAttributes_t *const)&cairoSurfaceAttributes,
                                                                        (const cairoPlotItem_t)       CAIRO_PLOT_ITEM_RECTANGLE,
                                                                        (const cairoPenColours_t)     CAIRO_PEN_ATTRIBUTE_COLOUR_RED,
                                                                        (const cairoPenIntensities_t) CAIRO_PEN_ATTRIBUTE_LIGHT,
                                                                                                      CAIRO_PEN_HALF_TRANSPARENT);

  cairo_rectangle(cr,
                  (BORDER_WIDTH * 2) - (line_width / 2.0), 
                  (BORDER_WIDTH * 2) - (line_width / 2.0),
                  (SNIPPET_WIDTH - (BORDER_WIDTH * 2)) + line_width,
                  (SNIPPET_WIDTH - (BORDER_WIDTH * 2)) + line_width);
  cairo_stroke(cr);

  /*****************************************************************************/
  /* Force all the previous drawing commands to draw their artefacts onto the  */
  /* drawing surface                                                           */
  /*****************************************************************************/

  cairo_surface_flush(surface);

  /*****************************************************************************/
  /* All done, clear and destroy the Cairo context and surface state           */
  /*****************************************************************************/

  cairo_destroy(cr);
  cairo_surface_destroy(surface);

  /*****************************************************************************/
  } /* end of on_paint                                                         */

/*****************************************************************************/
/* The WinMain and window procedure are loosely based on an example          */
/* from the Microsoft documentation.                                         */
/*****************************************************************************/

LRESULT CALLBACK WndProc(HWND   window,
                         UINT   message,
                         WPARAM wParam,
                         LPARAM lParam)
  {
  /*****************************************************************************/

  PAINTSTRUCT paint_struct;
  HDC         dc;

  /*****************************************************************************/

  switch (message) 
    {
    case WM_CHAR: switch (wParam) 
                    {
                    case 'q':
                    case 'Q':PostQuitMessage(0);
                             return(0);
                             break;
                    }
                  break;

    case WM_PAINT: dc = BeginPaint(window, &paint_struct);
                   on_paint(dc);
                   EndPaint(window, &paint_struct);
                   return(0);

    case WM_DESTROY: PostQuitMessage(0);
                     return(0);

    default:
      ;

    }

  return(DefWindowProc(window, message, wParam, lParam));

  /*****************************************************************************/
  } /* end of WndProc                                                          */

/*****************************************************************************/

#define WINDOW_STYLE WS_OVERLAPPEDWINDOW & ~(WS_MAXIMIZEBOX | WS_THICKFRAME)

/*****************************************************************************/

INT WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   PSTR      lpCmdLine,
                   INT       iCmdShow)
  {
  /*****************************************************************************/

  HWND     window;
  MSG      message;
  WNDCLASS window_class;
  RECT     rect;

  /*****************************************************************************/

  window_class.style         = CS_HREDRAW | CS_VREDRAW;
  window_class.lpfnWndProc   = WndProc;
  window_class.cbClsExtra    = 0;
  window_class.cbWndExtra    = 0;
  window_class.hInstance     = hInstance;
  window_class.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
  window_class.hCursor       = LoadCursor(NULL, IDC_ARROW);
  window_class.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
  window_class.lpszMenuName  = NULL;
  window_class.lpszClassName = TITLE;

  RegisterClass(&window_class);

  /* Compute the window size to give us the desired client area */
  rect.left   = 0;
  rect.top    = 0;
  rect.right  = (LONG)(SNIPPET_WIDTH + (2 * BORDER_WIDTH));
  rect.bottom = (LONG)(SNIPPET_WIDTH + (2 * BORDER_WIDTH));

  AdjustWindowRect(&rect, WINDOW_STYLE, FALSE /* no menu */);

  window = CreateWindow(TITLE, /* Class name */
                        TITLE, /* Window name */
                        WINDOW_STYLE,
                        CW_USEDEFAULT, CW_USEDEFAULT, /* initial position */
                        rect.right - rect.left, rect.bottom - rect.top, /* initial size */
                        NULL,	/* Parent */
                        NULL,	/* Menu */
                        hInstance,
                        NULL); /* WM_CREATE lpParam */

  ShowWindow(window, iCmdShow);
  UpdateWindow(window);

  while (GetMessage(&message, NULL, 0, 0)) 
    {
    TranslateMessage(&message);
    DispatchMessage(&message);
    }

  return((INT)message.wParam);

  /*****************************************************************************/
  } /* end of WinMain                                                          */

/*****************************************************************************/