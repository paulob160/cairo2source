/*****************************************************************************/
/* (C) Pulsing Core Software Limited 2021                                    */
/*****************************************************************************/
/*                                                                           */
/* File : CairoHelpers.c                                                     */
/* Date : 02.07.21                                                           */
/* By   : POB                                                                */
/* Description : encapsulation of Cairo functions to ease the use of calls   */
/*               necessarily made multiple times                             */
/*                                                                           */
/*****************************************************************************/

#include <windows.h>
#include <cairo-features.h>
#include <cairo-win32.h>
#include "CairoHelpers.h"

/*****************************************************************************/

const cairoPenColourTable_t penColourTable[CAIRO_PEN_ATTRIBUTE_COLOURS] =
  {
    {CAIRO_PEN_ATTRIBUTE_COLOUR_NONE,   {{ CAIRO_PEN_COLOUR_NONE_R,         CAIRO_PEN_COLOUR_NONE_G,         CAIRO_PEN_COLOUR_NONE_B },
                                         { CAIRO_PEN_COLOUR_NONE_R,         CAIRO_PEN_COLOUR_NONE_G,         CAIRO_PEN_COLOUR_NONE_B },
                                         { CAIRO_PEN_COLOUR_NONE_R,         CAIRO_PEN_COLOUR_NONE_G,         CAIRO_PEN_COLOUR_NONE_B }} },
    {CAIRO_PEN_ATTRIBUTE_COLOUR_WHITE,  {{ CAIRO_PEN_COLOUR_WHITE_R,        CAIRO_PEN_COLOUR_WHITE_G,        CAIRO_PEN_COLOUR_WHITE_B },
                                         { CAIRO_PEN_COLOUR_WHITE_R,        CAIRO_PEN_COLOUR_WHITE_G,        CAIRO_PEN_COLOUR_WHITE_B },
                                         { CAIRO_PEN_COLOUR_WHITE_R,        CAIRO_PEN_COLOUR_WHITE_G,        CAIRO_PEN_COLOUR_WHITE_B }} },
    {CAIRO_PEN_ATTRIBUTE_COLOUR_BLACK,  {{ CAIRO_PEN_COLOUR_BLACK_R,        CAIRO_PEN_COLOUR_BLACK_G,        CAIRO_PEN_COLOUR_BLACK_B },
                                         { CAIRO_PEN_COLOUR_BLACK_R,        CAIRO_PEN_COLOUR_BLACK_G,        CAIRO_PEN_COLOUR_BLACK_B },
                                         { CAIRO_PEN_COLOUR_BLACK_R,        CAIRO_PEN_COLOUR_BLACK_G,        CAIRO_PEN_COLOUR_BLACK_B }} },
    {CAIRO_PEN_ATTRIBUTE_COLOUR_RED,    {{ CAIRO_PEN_COLOUR_LIGHT_RED_R,    CAIRO_PEN_COLOUR_LIGHT_RED_G,    CAIRO_PEN_COLOUR_LIGHT_RED_B},
                                         { CAIRO_PEN_COLOUR_RED_R,          CAIRO_PEN_COLOUR_RED_G,          CAIRO_PEN_COLOUR_RED_B      }, 
                                         { CAIRO_PEN_COLOUR_DARK_RED_R,     CAIRO_PEN_COLOUR_RED_G,          CAIRO_PEN_COLOUR_RED_B      }} },
    {CAIRO_PEN_ATTRIBUTE_COLOUR_ORANGE, {{ CAIRO_PEN_COLOUR_LIGHT_ORANGE_R, CAIRO_PEN_COLOUR_LIGHT_ORANGE_G, CAIRO_PEN_COLOUR_LIGHT_ORANGE_B},
                                         { CAIRO_PEN_COLOUR_ORANGE_R,       CAIRO_PEN_COLOUR_ORANGE_G,       CAIRO_PEN_COLOUR_ORANGE_B      },
                                         { CAIRO_PEN_COLOUR_DARK_ORANGE_R,  CAIRO_PEN_COLOUR_DARK_ORANGE_G,  CAIRO_PEN_COLOUR_DARK_ORANGE_B }} },
    {CAIRO_PEN_ATTRIBUTE_COLOUR_YELLOW, {{ CAIRO_PEN_COLOUR_LIGHT_YELLOW_R, CAIRO_PEN_COLOUR_LIGHT_YELLOW_G, CAIRO_PEN_COLOUR_LIGHT_YELLOW_B},
                                         { CAIRO_PEN_COLOUR_YELLOW_R,       CAIRO_PEN_COLOUR_YELLOW_G,       CAIRO_PEN_COLOUR_YELLOW_B      },
                                         { CAIRO_PEN_COLOUR_DARK_YELLOW_R,  CAIRO_PEN_COLOUR_DARK_YELLOW_G,  CAIRO_PEN_COLOUR_DARK_YELLOW_B }} },
    {CAIRO_PEN_ATTRIBUTE_COLOUR_GREEN,  {{ CAIRO_PEN_COLOUR_LIGHT_GREEN_R,  CAIRO_PEN_COLOUR_LIGHT_GREEN_G,  CAIRO_PEN_COLOUR_LIGHT_GREEN_B},
                                         { CAIRO_PEN_COLOUR_GREEN_R,        CAIRO_PEN_COLOUR_GREEN_G,        CAIRO_PEN_COLOUR_GREEN_B      },
                                         { CAIRO_PEN_COLOUR_DARK_GREEN_R,   CAIRO_PEN_COLOUR_DARK_GREEN_G,   CAIRO_PEN_COLOUR_DARK_GREEN_B }} },
    {CAIRO_PEN_ATTRIBUTE_COLOUR_BLUE,   {{ CAIRO_PEN_COLOUR_LIGHT_BLUE_R,   CAIRO_PEN_COLOUR_LIGHT_BLUE_G,   CAIRO_PEN_COLOUR_LIGHT_BLUE_B},
                                         { CAIRO_PEN_COLOUR_BLUE_R,         CAIRO_PEN_COLOUR_BLUE_G,         CAIRO_PEN_COLOUR_BLUE_B      },
                                         { CAIRO_PEN_COLOUR_DARK_BLUE_R,    CAIRO_PEN_COLOUR_DARK_BLUE_G,    CAIRO_PEN_COLOUR_DARK_BLUE_B }} },
    {CAIRO_PEN_ATTRIBUTE_COLOUR_INDIGO, {{ CAIRO_PEN_COLOUR_LIGHT_INDIGO_R, CAIRO_PEN_COLOUR_LIGHT_INDIGO_G, CAIRO_PEN_COLOUR_LIGHT_INDIGO_B},
                                         { CAIRO_PEN_COLOUR_INDIGO_R,       CAIRO_PEN_COLOUR_INDIGO_G,       CAIRO_PEN_COLOUR_INDIGO_B      },
                                         { CAIRO_PEN_COLOUR_DARK_INDIGO_R,  CAIRO_PEN_COLOUR_DARK_INDIGO_G,  CAIRO_PEN_COLOUR_DARK_INDIGO_B }} },
    {CAIRO_PEN_ATTRIBUTE_COLOUR_VIOLET, {{ CAIRO_PEN_COLOUR_LIGHT_VIOLET_R, CAIRO_PEN_COLOUR_LIGHT_VIOLET_G, CAIRO_PEN_COLOUR_LIGHT_VIOLET_B},
                                         { CAIRO_PEN_COLOUR_VIOLET_R,       CAIRO_PEN_COLOUR_VIOLET_G,       CAIRO_PEN_COLOUR_VIOLET_B      },
                                         { CAIRO_PEN_COLOUR_DARK_VIOLET_R,  CAIRO_PEN_COLOUR_DARK_VIOLET_G,  CAIRO_PEN_COLOUR_DARK_VIOLET_B }} }
  };

// One instance of...
cairoSurfaceAttributes_t cairoSurfaceAttributes;

/*****************************************************************************/
/* cairoHelperSetCurrentPenColour() :                                        */
/*  <--> surface           : pointer to "Cairo" native surface handle        */
/*  <--> surfaceAttributes : pointer to surface helper attributes            */
/*   --> penColour         : pen colour of enum "cairoPenColours_t"          */
/*   --> plotItem          : associates the pen colour with a plotting type  */
/*                           e.g a rectangle form                            */
/*   --> penIntensity      : pen intensity of enum "cairoPenIntensities_t"   */

/*                                                                           */
/* - provides a more controlled wrapper for setting a cairo pen colour       */
/*                                                                           */
/*****************************************************************************/

cairoHelperErrorCode_t cairoHelperSetCurrentPenColour(cairo_t                  * const                       surface,
                                                      cairoSurfaceAttributes_t * const                       surfaceAttributes,
                                                                                 const cairoPlotItem_t       plotItem,
                                                                                 const cairoPenColours_t     penColour,
                                                                                 const cairoPenIntensities_t penIntensity,
                                                                                 const cairoDouble_t         penTransparency)
  {
  /*****************************************************************************/

  cairoHelperErrorCode_t errorCode = CAIRO_HELPER_PEN_ATTRIBUTES_ERROR_CODE;

  /*****************************************************************************/

  if ((surface          != NULL)                            && (surfaceAttributes  != NULL)                             && 
      ((penColour       >= CAIRO_PEN_ATTRIBUTE_COLOUR_NONE) && (penColour          <= CAIRO_PEN_ATTRIBUTE_COLOURS))     &&
      ((penIntensity    >= CAIRO_PEN_ATTRIBUTE_LIGHT)       && (penIntensity       <  CAIRO_PEN_ATTRIBUTE_INTENSITIES)) &&
      ((plotItem        >= CAIRO_PLOT_ITEM_GLOBAL)          && (plotItem           <  CAIRO_PLOT_ITEMS))                &&
      ((penTransparency >= CAIRO_PEN_ALPHA_MINIMUM)         && (penTransparency    <= CAIRO_PEN_ALPHA_MAXIMUM)))
    {
    surfaceAttributes->pen[plotItem].penColour                 = penColour;
    surfaceAttributes->pen[plotItem].penIntensity              = penIntensity;
    surfaceAttributes->pen[plotItem].penRGBA                   = penColourTable[penColour].penRGBA[penIntensity];
    surfaceAttributes->pen[plotItem].penRGBA.penAlphaComponent = penTransparency;

    cairo_set_source_rgba(surface, penColourTable[penColour].penRGBA[penIntensity].penRedComponent, 
                                   penColourTable[penColour].penRGBA[penIntensity].penGreenComponent, 
                                   penColourTable[penColour].penRGBA[penIntensity].penBlueComponent,
                                   penTransparency);

    errorCode = CAIRO_HELPER_ERROR_CODE_NONE;
    };

  /*****************************************************************************/

  return(errorCode);

  /*****************************************************************************/
  } /* end of cairoHelperSetCurrentPenColour                                   */

/*****************************************************************************/
/* cairoHelperReadCurrentPenColour() :                                       */
/*  <--> surface           : pointer to "Cairo" native surface handle        */
/*  <--> surfaceAttributes : pointer to surface helper attributes            */
/*   --> plotItem          : associates the pen colour with a plotting type  */
/*                           e.g a rectangle form                            */
/*   --> penColour         : pen colour of enum "cairoPenColours_t"          */
/*   --> penIntensity      : pen intensity of enum "cairoPenIntensities_t"   */
/*   --> penRGBA           : pen red, green, blue and alpha values           */
/*                                                                           */
/* - provides a wrapper for reading the current cairo pen colour, intensity  */
/*   and red, green, blue and alpha channel values for this plot type        */
/*                                                                           */
/*****************************************************************************/

cairoHelperErrorCode_t cairoHelperReadCurrentPenColour(cairo_t                  * const                 surface,
                                                       cairoSurfaceAttributes_t * const                 surfaceAttributes,
                                                                                  const cairoPlotItem_t plotItem,
                                                       cairoPenColours_t        * const                 penColour,
                                                       cairoPenIntensities_t    * const                 penIntensity,
                                                       cairoPenRBGA_t           * const                 penRGBA)
  {
  /*****************************************************************************/

  cairoHelperErrorCode_t errorCode = CAIRO_HELPER_PEN_ATTRIBUTES_ERROR_CODE;

  /*****************************************************************************/

  if (((surface           != NULL)                   && (surfaceAttributes != NULL)) &&
      ((penColour         != NULL)                   && (penIntensity      != NULL)) &&
       (penRGBA           != NULL)                   &&
      ((plotItem          >= CAIRO_PLOT_ITEM_GLOBAL) && (plotItem          <  CAIRO_PLOT_ITEMS)))
    {
    *penColour    = surfaceAttributes->pen[plotItem].penColour;
    *penIntensity = surfaceAttributes->pen[plotItem].penIntensity;
    *penRGBA      = surfaceAttributes->pen[plotItem].penRGBA;

    errorCode     = CAIRO_HELPER_ERROR_CODE_NONE;
    }

  /*****************************************************************************/

  return(errorCode);

  /*****************************************************************************/
  } /* end of cairoHelperReadCurrentPenColour                                  */

/*****************************************************************************/
/* (C) Pulsing Core Software Limited 2021                                    */
/*****************************************************************************/