/*****************************************************************************/
/* (C) Pulsing Core Software Limited 2021                                    */
/*****************************************************************************/
/*                                                                           */
/* File : CairoHelpers.h                                                     */
/* Date : 02.07.21                                                           */
/* By   : POB                                                                */
/* Description : encapsulation of Cairo functions to ease the use of calls   */
/*               necessarily made multiple times                             */
/*                                                                           */
/*****************************************************************************/

#ifndef _CAIRO_HELPERS_H_
#define _CAIRO_HELPERS_H_

/*****************************************************************************/

#include <stdbool.h>
#include <stdint.h>

/*****************************************************************************/

#define CAIRO_HELPER_MAXIMUM_PEN_ATTRIBUTES (30)  
#define CAIRO_HELPER_RGBA_COMPONENTS         (4)

// RGB colour maximum and minimum byte values
#define CAIRO_PEN_COLOUR_RED_MAX            ((cairoUnsignedByte_t)0x01) // ((cairoUnsignedByte_t)0xFF)
#define CAIRO_PEN_COLOUR_GREEN_MAX          ((cairoUnsignedByte_t)0x01) // ((cairoUnsignedByte_t)0xFF)
#define CAIRO_PEN_COLOUR_BLUE_MAX           ((cairoUnsignedByte_t)0x01) // ((cairoUnsignedByte_t)0xFF)

#define CAIRO_PEN_COLOUR_RED_MIN            ((cairoUnsignedByte_t)0x00)
#define CAIRO_PEN_COLOUR_GREEN_MIN          ((cairoUnsignedByte_t)0x00)
#define CAIRO_PEN_COLOUR_BLUE_MIN           ((cairoUnsignedByte_t)0x00)

// Colour : none
#define CAIRO_PEN_COLOUR_NONE_R             ((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)
#define CAIRO_PEN_COLOUR_NONE_G             ((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX)
#define CAIRO_PEN_COLOUR_NONE_B             ((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)

// Colour : white
#define CAIRO_PEN_COLOUR_WHITE_R            ((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)
#define CAIRO_PEN_COLOUR_WHITE_G            ((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX)
#define CAIRO_PEN_COLOUR_WHITE_B            ((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)

// Colour : black
#define CAIRO_PEN_COLOUR_BLACK_R            ((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MIN)
#define CAIRO_PEN_COLOUR_BLACK_G            ((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MIN)
#define CAIRO_PEN_COLOUR_BLACK_B            ((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MIN)

// Colour : light red, red, dark red
#define CAIRO_PEN_COLOUR_LIGHT_RED_Ra       ((cairoDouble_t)1.00)
#define CAIRO_PEN_COLOUR_LIGHT_RED_Ga       ((cairoDouble_t)0.39)
#define CAIRO_PEN_COLOUR_LIGHT_RED_Ba       ((cairoDouble_t)0.28)
#define CAIRO_PEN_COLOUR_RED                ((cairoDouble_t)1.00)
#define CAIRO_PEN_COLOUR_DARK_RED           ((cairoDouble_t)0.55)

#define CAIRO_PEN_COLOUR_LIGHT_RED_R        (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_LIGHT_RED_Ra)
#define CAIRO_PEN_COLOUR_LIGHT_RED_G        (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_LIGHT_RED_Ga)
#define CAIRO_PEN_COLOUR_LIGHT_RED_B        (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_LIGHT_RED_Ba)

#define CAIRO_PEN_COLOUR_RED_R              (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX) * CAIRO_PEN_COLOUR_RED)
#define CAIRO_PEN_COLOUR_DARK_RED_R         (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX) * CAIRO_PEN_COLOUR_DARK_RED)

#define CAIRO_PEN_COLOUR_RED_G              ((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MIN)
#define CAIRO_PEN_COLOUR_RED_B              ((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MIN)

// Colour : light orange, orange, dark orange
#define CAIRO_PEN_COLOUR_LIGHT_ORANGE_Ra    ((cairoDouble_t)0.98)
#define CAIRO_PEN_COLOUR_LIGHT_ORANGE_Ga    ((cairoDouble_t)0.50)
#define CAIRO_PEN_COLOUR_LIGHT_ORANGE_Ba    ((cairoDouble_t)0.45)

#define CAIRO_PEN_COLOUR_ORANGE_Ra          ((cairoDouble_t)1.00)
#define CAIRO_PEN_COLOUR_ORANGE_Ga          ((cairoDouble_t)0.65)
#define CAIRO_PEN_COLOUR_ORANGE_Ba          ((cairoDouble_t)0.00)

#define CAIRO_PEN_COLOUR_DARK_ORANGE_Ra     ((cairoDouble_t)1.00)
#define CAIRO_PEN_COLOUR_DARK_ORANGE_Ga     ((cairoDouble_t)0.55)
#define CAIRO_PEN_COLOUR_DARK_ORANGE_Ba     ((cairoDouble_t)0.00)

#define CAIRO_PEN_COLOUR_LIGHT_ORANGE_R     (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_LIGHT_ORANGE_Ra)
#define CAIRO_PEN_COLOUR_LIGHT_ORANGE_G     (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_LIGHT_ORANGE_Ga)
#define CAIRO_PEN_COLOUR_LIGHT_ORANGE_B     (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_LIGHT_ORANGE_Ba)
                                            
#define CAIRO_PEN_COLOUR_ORANGE_R           (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_ORANGE_Ra)
#define CAIRO_PEN_COLOUR_ORANGE_G           (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_ORANGE_Ga)
#define CAIRO_PEN_COLOUR_ORANGE_B           (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_ORANGE_Ba)

#define CAIRO_PEN_COLOUR_DARK_ORANGE_R      (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_DARK_ORANGE_Ra)
#define CAIRO_PEN_COLOUR_DARK_ORANGE_G      (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_DARK_ORANGE_Ga)
#define CAIRO_PEN_COLOUR_DARK_ORANGE_B      (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_DARK_ORANGE_Ba)

// Colour : light yellow, yellow, dark yellow
#define CAIRO_PEN_COLOUR_LIGHT_YELLOW_Ra    ((cairoDouble_t)1.00)
#define CAIRO_PEN_COLOUR_LIGHT_YELLOW_Ga    ((cairoDouble_t)1.00)
#define CAIRO_PEN_COLOUR_LIGHT_YELLOW_Ba    ((cairoDouble_t)0.88)

#define CAIRO_PEN_COLOUR_YELLOW_Ra          ((cairoDouble_t)1.00)
#define CAIRO_PEN_COLOUR_YELLOW_Ga          ((cairoDouble_t)1.00)
#define CAIRO_PEN_COLOUR_YELLOW_Ba          ((cairoDouble_t)0.00)

#define CAIRO_PEN_COLOUR_DARK_YELLOW_Ra     ((cairoDouble_t)1.00)
#define CAIRO_PEN_COLOUR_DARK_YELLOW_Ga     ((cairoDouble_t)0.84)
#define CAIRO_PEN_COLOUR_DARK_YELLOW_Ba     ((cairoDouble_t)0.00)

#define CAIRO_PEN_COLOUR_LIGHT_YELLOW_R     (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_LIGHT_YELLOW_Ra)
#define CAIRO_PEN_COLOUR_LIGHT_YELLOW_G     (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_LIGHT_YELLOW_Ga)
#define CAIRO_PEN_COLOUR_LIGHT_YELLOW_B     (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_LIGHT_YELLOW_Ba)
                                            
#define CAIRO_PEN_COLOUR_YELLOW_R           (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_YELLOW_Ra)
#define CAIRO_PEN_COLOUR_YELLOW_G           (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_YELLOW_Ga)
#define CAIRO_PEN_COLOUR_YELLOW_B           (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_YELLOW_Ba)

#define CAIRO_PEN_COLOUR_DARK_YELLOW_R      (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_DARK_YELLOW_Ra)
#define CAIRO_PEN_COLOUR_DARK_YELLOW_G      (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_DARK_YELLOW_Ga)
#define CAIRO_PEN_COLOUR_DARK_YELLOW_B      (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_DARK_YELLOW_Ba)

// Colour : light green, green, dark green
#define CAIRO_PEN_COLOUR_LIGHT_GREEN_Ra     ((cairoDouble_t)0.56)
#define CAIRO_PEN_COLOUR_LIGHT_GREEN_Ga     ((cairoDouble_t)0.93)
#define CAIRO_PEN_COLOUR_LIGHT_GREEN_Ba     ((cairoDouble_t)0.56)
                                           
#define CAIRO_PEN_COLOUR_GREEN_Ra           ((cairoDouble_t)0.00)
#define CAIRO_PEN_COLOUR_GREEN_Ga           ((cairoDouble_t)0.50)
#define CAIRO_PEN_COLOUR_GREEN_Ba           ((cairoDouble_t)0.00)
                                           
#define CAIRO_PEN_COLOUR_DARK_GREEN_Ra      ((cairoDouble_t)0.00)
#define CAIRO_PEN_COLOUR_DARK_GREEN_Ga      ((cairoDouble_t)0.39)
#define CAIRO_PEN_COLOUR_DARK_GREEN_Ba      ((cairoDouble_t)0.00)
                                           
#define CAIRO_PEN_COLOUR_LIGHT_GREEN_R      (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_LIGHT_GREEN_Ra)
#define CAIRO_PEN_COLOUR_LIGHT_GREEN_G      (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_LIGHT_GREEN_Ga)
#define CAIRO_PEN_COLOUR_LIGHT_GREEN_B      (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_LIGHT_GREEN_Ba)
                                             
#define CAIRO_PEN_COLOUR_GREEN_R            (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_GREEN_Ra)
#define CAIRO_PEN_COLOUR_GREEN_G            (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_GREEN_Ga)
#define CAIRO_PEN_COLOUR_GREEN_B            (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_GREEN_Ba)
                                           
#define CAIRO_PEN_COLOUR_DARK_GREEN_R       (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_DARK_GREEN_Ra)
#define CAIRO_PEN_COLOUR_DARK_GREEN_G       (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_DARK_GREEN_Ga)
#define CAIRO_PEN_COLOUR_DARK_GREEN_B       (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_DARK_GREEN_Ba)

// Colour : light blue, blue, dark blue
#define CAIRO_PEN_COLOUR_LIGHT_BLUE_Ra      ((cairoDouble_t)0.68)
#define CAIRO_PEN_COLOUR_LIGHT_BLUE_Ga      ((cairoDouble_t)0.85)
#define CAIRO_PEN_COLOUR_LIGHT_BLUE_Ba      ((cairoDouble_t)0.90)
                                            
#define CAIRO_PEN_COLOUR_BLUE_Ra            ((cairoDouble_t)0.00)
#define CAIRO_PEN_COLOUR_BLUE_Ga            ((cairoDouble_t)0.00)
#define CAIRO_PEN_COLOUR_BLUE_Ba            ((cairoDouble_t)1.00)
                                            
#define CAIRO_PEN_COLOUR_DARK_BLUE_Ra       ((cairoDouble_t)0.00)
#define CAIRO_PEN_COLOUR_DARK_BLUE_Ga       ((cairoDouble_t)0.00)
#define CAIRO_PEN_COLOUR_DARK_BLUE_Ba       ((cairoDouble_t)0.55)
                                            
#define CAIRO_PEN_COLOUR_LIGHT_BLUE_R       (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_LIGHT_BLUE_Ra)
#define CAIRO_PEN_COLOUR_LIGHT_BLUE_G       (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_LIGHT_BLUE_Ga)
#define CAIRO_PEN_COLOUR_LIGHT_BLUE_B       (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_LIGHT_BLUE_Ba)
                                              
#define CAIRO_PEN_COLOUR_BLUE_R             (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_BLUE_Ra)
#define CAIRO_PEN_COLOUR_BLUE_G             (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_BLUE_Ga)
#define CAIRO_PEN_COLOUR_BLUE_B             (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_BLUE_Ba)
                                            
#define CAIRO_PEN_COLOUR_DARK_BLUE_R        (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_DARK_BLUE_Ra)
#define CAIRO_PEN_COLOUR_DARK_BLUE_G        (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_DARK_BLUE_Ga)
#define CAIRO_PEN_COLOUR_DARK_BLUE_B        (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_DARK_BLUE_Ba)

// Colour : light indigo (dark slate blue), indigo, dark indigo (midnight blue)
#define CAIRO_PEN_COLOUR_LIGHT_INDIGO_Ra      ((cairoDouble_t)0.28)
#define CAIRO_PEN_COLOUR_LIGHT_INDIGO_Ga      ((cairoDouble_t)0.24)
#define CAIRO_PEN_COLOUR_LIGHT_INDIGO_Ba      ((cairoDouble_t)0.55)
                                            
#define CAIRO_PEN_COLOUR_INDIGO_Ra            ((cairoDouble_t)0.29)
#define CAIRO_PEN_COLOUR_INDIGO_Ga            ((cairoDouble_t)0.00)
#define CAIRO_PEN_COLOUR_INDIGO_Ba            ((cairoDouble_t)0.51)
                                            
#define CAIRO_PEN_COLOUR_DARK_INDIGO_Ra       ((cairoDouble_t)0.10)
#define CAIRO_PEN_COLOUR_DARK_INDIGO_Ga       ((cairoDouble_t)0.10)
#define CAIRO_PEN_COLOUR_DARK_INDIGO_Ba       ((cairoDouble_t)0.44)
                                            
#define CAIRO_PEN_COLOUR_LIGHT_INDIGO_R       (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_LIGHT_INDIGO_Ra)
#define CAIRO_PEN_COLOUR_LIGHT_INDIGO_G       (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_LIGHT_INDIGO_Ga)
#define CAIRO_PEN_COLOUR_LIGHT_INDIGO_B       (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_LIGHT_INDIGO_Ba)
                                              
#define CAIRO_PEN_COLOUR_INDIGO_R             (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_INDIGO_Ra)
#define CAIRO_PEN_COLOUR_INDIGO_G             (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_INDIGO_Ga)
#define CAIRO_PEN_COLOUR_INDIGO_B             (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_INDIGO_Ba)
                                            
#define CAIRO_PEN_COLOUR_DARK_INDIGO_R        (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_DARK_INDIGO_Ra)
#define CAIRO_PEN_COLOUR_DARK_INDIGO_G        (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_DARK_INDIGO_Ga)
#define CAIRO_PEN_COLOUR_DARK_INDIGO_B        (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_DARK_INDIGO_Ba)

// Colour : light violet (thistle), violet, dark violet (blue violet)
#define CAIRO_PEN_COLOUR_LIGHT_VIOLET_Ra      ((cairoDouble_t)0.85)
#define CAIRO_PEN_COLOUR_LIGHT_VIOLET_Ga      ((cairoDouble_t)0.75)
#define CAIRO_PEN_COLOUR_LIGHT_VIOLET_Ba      ((cairoDouble_t)0.85)
                                            
#define CAIRO_PEN_COLOUR_VIOLET_Ra            ((cairoDouble_t)0.93)
#define CAIRO_PEN_COLOUR_VIOLET_Ga            ((cairoDouble_t)0.51)
#define CAIRO_PEN_COLOUR_VIOLET_Ba            ((cairoDouble_t)0.93)
                                            
#define CAIRO_PEN_COLOUR_DARK_VIOLET_Ra       ((cairoDouble_t)0.54)
#define CAIRO_PEN_COLOUR_DARK_VIOLET_Ga       ((cairoDouble_t)0.17)
#define CAIRO_PEN_COLOUR_DARK_VIOLET_Ba       ((cairoDouble_t)0.89)
                                            
#define CAIRO_PEN_COLOUR_LIGHT_VIOLET_R       (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_LIGHT_VIOLET_Ra)
#define CAIRO_PEN_COLOUR_LIGHT_VIOLET_G       (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_LIGHT_VIOLET_Ga)
#define CAIRO_PEN_COLOUR_LIGHT_VIOLET_B       (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_LIGHT_VIOLET_Ba)
                                              
#define CAIRO_PEN_COLOUR_VIOLET_R             (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_VIOLET_Ra)
#define CAIRO_PEN_COLOUR_VIOLET_G             (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_VIOLET_Ga)
#define CAIRO_PEN_COLOUR_VIOLET_B             (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_VIOLET_Ba)
                                            
#define CAIRO_PEN_COLOUR_DARK_VIOLET_R        (((cairoDouble_t)CAIRO_PEN_COLOUR_RED_MAX)   * CAIRO_PEN_COLOUR_DARK_VIOLET_Ra)
#define CAIRO_PEN_COLOUR_DARK_VIOLET_G        (((cairoDouble_t)CAIRO_PEN_COLOUR_BLUE_MAX)  * CAIRO_PEN_COLOUR_DARK_VIOLET_Ga)
#define CAIRO_PEN_COLOUR_DARK_VIOLET_B        (((cairoDouble_t)CAIRO_PEN_COLOUR_GREEN_MAX) * CAIRO_PEN_COLOUR_DARK_VIOLET_Ba)

#define CAIRO_PEN_ALPHA_MINIMUM               ((const cairoDouble_t)0.0)
#define CAIRO_PEN_ALPHA_TENTH                 ((const cairoDouble_t)0.10)
#define CAIRO_PEN_ALPHA_QUARTER               ((const cairoDouble_t)0.25)
#define CAIRO_PEN_ALPHA_HALF                  ((const cairoDouble_t)0.50)
#define CAIRO_PEN_ALPHA_THREE_QUARTERS        ((const cairoDouble_t)0.75)
#define CAIRO_PEN_ALPHA_NINE_TENTHS           ((const cairoDouble_t)0.90)
#define CAIRO_PEN_ALPHA_MAXIMUM               ((const cairoDouble_t)1.0)

#define CAIRO_PEN_FULLY_TRANSPARENT           CAIRO_PEN_ALPHA_MINIMUM
#define CAIRO_PEN_NEARLY_TRANSPARENT          CAIRO_PEN_ALPHA_TENTH
#define CAIRO_PEN_MOSTLY_TRANSPARENT          CAIRO_PEN_ALPHA_QUARTER
#define CAIRO_PEN_HALF_TRANSPARENT            CAIRO_PEN_ALPHA_HALF
#define CAIRO_PEN_HALF_OPAQUE                 CAIRO_PEN_ALPHA_HALF
#define CAIRO_PEN_MOSTLY_OPAQUE               CAIRO_PEN_ALPHA_THREE_QUARTERS
#define CAIRO_PEN_NEARLY_OPAQUE               CAIRO_PEN_ALPHA_NINE_TENTHS
#define CAIRO_PEN_FULLY_OPAQUE                CAIRO_PEN_ALPHA_MAXIMUM

/*****************************************************************************/

typedef uint8_t cairoUnsignedByte_t;
typedef double  cairoDouble_t;

typedef enum cairoHelperErrorCode_tTag
  {
  CAIRO_HELPER_ERROR_CODE_NONE     = 0,
  CAIRO_HELPER_PEN_ATTRIBUTES_ERROR_CODE,
  CAIRO_HELPER_ERROR_CODES
  } cairoHelperErrorCode_t;

typedef enum cairoPenAttributes_tTag
  {
  // MAXIMUM OF 30 ENTRIES + "CAIRO_HELPER_MAXIMUM_PEN_ATTRIBUTES"
  CAIRO_PEN_ATTRIBUTE_RESET         = 1,
  CAIRO_PEN_ATTRIBUTE_TRANSPARENT   = (CAIRO_PEN_ATTRIBUTE_RESET         << 1),
  CAIRO_PEN_ATTRIBUTE_TRANSPARENT_N = (CAIRO_PEN_ATTRIBUTE_TRANSPARENT   << 1),

  CAIRO_PEN_ATTRIBUTES
  } cairoPenAttributes_t;

typedef enum cairoPenColours_tTag
  {
  CAIRO_PEN_ATTRIBUTE_COLOUR_NONE   = 0,
  CAIRO_PEN_ATTRIBUTE_COLOUR_WHITE  = (CAIRO_PEN_ATTRIBUTE_COLOUR_NONE   + 1),
  CAIRO_PEN_ATTRIBUTE_COLOUR_BLACK  = (CAIRO_PEN_ATTRIBUTE_COLOUR_WHITE  + 1),
  CAIRO_PEN_ATTRIBUTE_COLOUR_RED    = (CAIRO_PEN_ATTRIBUTE_COLOUR_BLACK  + 1),
  CAIRO_PEN_ATTRIBUTE_COLOUR_ORANGE = (CAIRO_PEN_ATTRIBUTE_COLOUR_RED    + 1),
  CAIRO_PEN_ATTRIBUTE_COLOUR_YELLOW = (CAIRO_PEN_ATTRIBUTE_COLOUR_ORANGE + 1),
  CAIRO_PEN_ATTRIBUTE_COLOUR_GREEN  = (CAIRO_PEN_ATTRIBUTE_COLOUR_YELLOW + 1),
  CAIRO_PEN_ATTRIBUTE_COLOUR_BLUE   = (CAIRO_PEN_ATTRIBUTE_COLOUR_GREEN  + 1),
  CAIRO_PEN_ATTRIBUTE_COLOUR_INDIGO = (CAIRO_PEN_ATTRIBUTE_COLOUR_BLUE   + 1),
  CAIRO_PEN_ATTRIBUTE_COLOUR_VIOLET = (CAIRO_PEN_ATTRIBUTE_COLOUR_INDIGO + 1),
  CAIRO_PEN_ATTRIBUTE_COLOURS       = (CAIRO_PEN_ATTRIBUTE_COLOUR_VIOLET + 1)
  } cairoPenColours_t;

typedef enum cairoPenIntensities_tTag
  {
  CAIRO_PEN_ATTRIBUTE_LIGHT = 0,
  CAIRO_PEN_ATTRIBUTE_NORMAL,
  CAIRO_PEN_ATTRIBUTE_DARK,
  CAIRO_PEN_ATTRIBUTE_INTENSITIES   
  } cairoPenIntensities_t;

typedef struct cairoPenRBGA_tTag
  {
  cairoDouble_t penRedComponent;
  cairoDouble_t penGreenComponent;
  cairoDouble_t penBlueComponent;
  cairoDouble_t penAlphaComponent;
  } cairoPenRBGA_t;

typedef struct cairoPenColourTable_tTag
  {
  cairoPenColours_t     penColour;
  //cairoPenIntensities_t penIntensity;
  cairoPenRBGA_t        penRGBA[CAIRO_PEN_ATTRIBUTE_INTENSITIES];
  } cairoPenColourTable_t;

typedef enum cairoPlotItem_tTag
  {
  CAIRO_PLOT_ITEM_GLOBAL = 0,
  CAIRO_PLOT_ITEM_TEXT,
  CAIRO_PLOT_ITEM_RECTANGLE,
  CAIRO_PLOT_ITEMS
  } cairoPlotItem_t;

typedef struct cairoPen_tTag
  {
  cairoPenColours_t     penColour;
  cairoPenIntensities_t penIntensity;
  cairoPenAttributes_t  penTransparency;
  cairoPenRBGA_t        penRGBA;
  } cairoPen_t; 

typedef struct cairoSurfaceAttributes_tTag
  {
  cairoPen_t pen[CAIRO_PLOT_ITEMS];
  } cairoSurfaceAttributes_t;

/*****************************************************************************/

extern const cairoPenColourTable_t    penColourTable[CAIRO_PEN_ATTRIBUTE_COLOURS];
extern       cairoSurfaceAttributes_t cairoSurfaceAttributes;

/*****************************************************************************/

extern cairoHelperErrorCode_t cairoHelperSetCurrentPenColour(cairo_t                  * const                       surface,
                                                             cairoSurfaceAttributes_t * const                       surfaceAttributes,
                                                                                        const cairoPlotItem_t       plotItem,
                                                                                        const cairoPenColours_t     penColour,
                                                                                        const cairoPenIntensities_t penIntensity,
                                                                                        const cairoDouble_t         penTransparency);
extern cairoHelperErrorCode_t cairoHelperReadCurrentPenColour(cairo_t                  * const                 surface, 
                                                              cairoSurfaceAttributes_t * const                 surfaceAttributes,
                                                                                         const cairoPlotItem_t plotItem,
                                                              cairoPenColours_t        * const                 penColour,
                                                              cairoPenIntensities_t    * const                 penIntensity,
                                                              cairoPenRBGA_t           * const                 penRGBA);

/*****************************************************************************/

#endif

/*****************************************************************************/
/* (C) Pulsing Core Software Limited 2021                                    */
/*****************************************************************************/
